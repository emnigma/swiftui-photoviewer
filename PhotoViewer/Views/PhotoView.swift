//
//  PhotoView.swift
//  PhotoViewer
//
//  Created by Max Nigmatulin on 17.07.2022.
//

import SwiftUI

struct PhotoView: View {
    @ObservedObject var remote: Remote<Image>
    let author: String
    
    init(author: String, photoURL: String) {
        self.author = author
        self.remote = Remote(remote: photoURL, transform: {
            guard let uiImage = UIImage(data: $0)
            else { return nil }
            return Image(uiImage: uiImage)
        })
    }
    
    var body: some View {
        Group {
            if let image = remote.value {
                image
                    .resizable()
                    .scaledToFit()
                    .padding()
            }
            else {
                ActivityIndicator()
                    .onAppear {
                        remote.load()
                    }
            }
        }
        .navigationTitle(author)
    }
}

struct PhotoView_Previews: PreviewProvider {
    static var previews: some View {
        PhotoView(author: "Matthew Wiebe", photoURL: "https://picsum.photos/id/1025/4951/3301")
    }
}
