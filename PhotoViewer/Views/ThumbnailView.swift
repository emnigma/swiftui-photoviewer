//
//  ThumbnailView.swift
//  PhotoViewer
//
//  Created by Max Nigmatulin on 17.07.2022.
//

import SwiftUI

struct ThumbnailView: View {
    @ObservedObject var remote: Remote<Image>

    init(photoURL: String) {
        self.remote = Remote(remote: photoURL, transform: {
            guard let uiImage = UIImage(data: $0),
                  let resizedUiImage = uiImage.thumbnailed(size: 25)
            else { return nil }
            return Image(uiImage: resizedUiImage)
        })
    }

    @ViewBuilder var body: some View {
        if let thumbnail = remote.value {
            thumbnail
                .cornerRadius(3)
                .frame(width: 25, height: 25)
        }
        else {
            ProgressView()
                .frame(width: 25, height: 25)
                .onAppear {
                    remote.load()
                }
        }
    }
}

struct ThumbnailView_Previews: PreviewProvider {
    static var previews: some View {
        ThumbnailView(photoURL: "https://picsum.photos/id/1025/4951/3301")
    }
}
