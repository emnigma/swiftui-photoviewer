//
//  ContentView.swift
//  PhotoViewer
//
//  Created by Max Nigmatulin on 17.07.2022.
//

import SwiftUI

struct ContentView: View {

    @ObservedObject var remote = Remote(remote: "https://picsum.photos/v2/list", transform: {
        try? JSONDecoder().decode([PhotoModel].self, from: $0)
    })
    
    var body: some View {
        NavigationView {
            authorList
                .navigationTitle("Author List")
        }

    }

    @ViewBuilder var authorList: some View {
        if let photos = remote.value {
            List {
                ForEach(photos) { photo in
                    NavigationLink(
                        destination: PhotoView(author: photo.author, photoURL: photo.download_url)
                    ) {
                        HStack {
                            ThumbnailView(photoURL: photo.download_url)
                            Text(photo.author)
                        }
                    }
                }
            }
        }
        else {
            Text("Loading...")
                .onAppear {
                    remote.load()
                }
        }
    }
}


// experiment
struct ActivityIndicator: UIViewRepresentable {

    func makeUIView(context: Context) -> UIActivityIndicatorView {
        let v = UIActivityIndicatorView()

        return v
    }

    func updateUIView(_ activityIndicator: UIActivityIndicatorView, context: Context) {
        activityIndicator.startAnimating()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
