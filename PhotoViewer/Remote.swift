//
//  Remote.swift
//  PhotoViewer
//
//  Created by Max Nigmatulin on 17.07.2022.
//

import Foundation

enum LoadingError: Error {
    case hz
}

class Remote<T>: ObservableObject {
    @Published var result: Result<T, Error>? = nil
    var value: T? {
        try? result?.get()
    }
        
    let remoteURL: URL
    let transform: (Data) -> T?
    
    init(remote: String, transform: @escaping (Data) -> T?) {
        self.remoteURL = URL(string: remote)!
        self.transform = transform
    }
    
    func load() {
        URLSession.shared
            .dataTask(with: remoteURL) { raw, _, error in
            DispatchQueue.main.async {
                if let raw = raw,
                   let transformed = self.transform(raw) {
                    self.result = .success(transformed)
                }
                else {
                    if let error = error {
                        self.result = .failure(error)
                    }
                    else {
                        self.result = .failure(LoadingError.hz)
                    }
                }
            }
        }.resume()
    }
}
