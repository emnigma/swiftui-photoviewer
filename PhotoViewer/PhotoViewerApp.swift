//
//  PhotoViewerApp.swift
//  PhotoViewer
//
//  Created by Max Nigmatulin on 17.07.2022.
//

import SwiftUI

@main
struct PhotoViewerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
