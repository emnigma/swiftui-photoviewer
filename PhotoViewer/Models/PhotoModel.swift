//
//  PhotoModel.swift
//  PhotoViewer
//
//  Created by Max Nigmatulin on 17.07.2022.
//

struct PhotoModel: Codable, Identifiable {
    let id: String
    let author: String
    let width: Int
    let height: Int
    let url: String
    let download_url: String
}
